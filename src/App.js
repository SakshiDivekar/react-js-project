import { Route, Switch } from 'react-router-dom';
import AllMeetUpsPage from './pages/AllMeetUps';
import FavoritesPage from './pages/Favorites';
import NewMeetUpsPage from './pages/NewMeetUps';
import Layout from './components/layout/Layout';


function App() {
  return (
  
      <Layout>
        
        <Switch>
        <Route path='/' exact={true}>
          <AllMeetUpsPage />
        </Route>
        <Route path='/new-meetup'>
          <NewMeetUpsPage />
        </Route>
        <Route path='/favorites'>
          <FavoritesPage />
        </Route>
        </Switch>
      </Layout>
  
  );
}

export default App;








































/*import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
*/
